#ifndef GSTREAMER_CONTEXT_H
#define GSTREAMER_CONTEXT_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
} GstreamerContext;

GstreamerContext *GstreamerContext_Create(uint8_t *buffer, size_t size);
void GstreamerContext_Destroy(GstreamerContext *gstreamer);

void GstreamerContext_Encode(GstreamerContext *gstreamer);
void GstreamerContext_Dncode(GstreamerContext *gstreamer);

#endif
