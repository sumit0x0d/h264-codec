#ifndef FFMPEG_CONTEXT_H
#define FFMPEG_CONTEXT_H

#include <stddef.h>
#include <stdint.h>

typedef struct {
} FfmpegContext;

FfmpegContext *FfmpegContext_Create(uint8_t *buffer, size_t size);
void FfmpegContext_Destroy(FfmpegContext *ffmpeg);

void FfmpegContext_Encode(FfmpegContext *ffmpeg);
void FfmpegContext_Dncode(FfmpegContext *ffmpeg);

#endif
